Preview
<hr>
Login page

![Login page](https://picc.io/9Ysi4wM.jpg)

<hr>
Home page

![Home page](https://picc.io/QO2fd7M.jpg)

<hr>
Edit task

![Edit task](https://picc.io/DDzFH1e.jpg)

<hr>
Delete task

![Delete task](https://picc.io/Ghwlkae.jpg)
![Delete task confirmation](https://picc.io/doBpyGw.jpg)

<hr>
Logout

![Logout](https://picc.io/UJGGKMD.jpg)
