package com.example.tasks.Controller;

import android.content.Context;
import android.content.SharedPreferences;
import android.util.Log;

import com.example.tasks.Model.UserModel;
import com.example.tasks.Util.DatabaseHandler;

public class AuthController {
    private DatabaseHandler db;

    public AuthController(DatabaseHandler db) {
        this.db = db;
    }

    public boolean register(UserModel user) {
        return db.createUser(user);
    }

    public int login(UserModel user) {
        return db.authUser(user);
    }
}
