package com.example.tasks.Util;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.provider.BaseColumns;
import android.util.Log;

import com.example.tasks.Model.ToDoModel;
import com.example.tasks.Model.UserModel;

import java.util.ArrayList;
import java.util.List;

public class DatabaseHandler extends SQLiteOpenHelper {
    private static final int VERSION = 1;
    private static final String NAME = "todoListDatabase";
    private static final String TODO_TABLE = "todo";
    private static final String ID = "id";
    private static final String TASK = "task";
    private static final String STATUS = "status";
    private static final String CREATE_TODO_TABLE = "CREATE TABLE " + TODO_TABLE + " ("
            + ID + " INTEGER PRIMARY KEY AUTOINCREMENT," + TASK + " TEXT, " + STATUS + " INTEGER)";

    private static final String USER_TABLE = "user";
    private static final String USERNAME = "username";
    private static final String PASSWORD = "password";
    private static final String CREATE_USER_TABLE = "CREATE TABLE " + USER_TABLE + " ("
            + ID + " INTEGER PRIMARY KEY AUTOINCREMENT," + USERNAME + " TEXT, " + PASSWORD + " TEXT)";

    private SQLiteDatabase db;

    public DatabaseHandler(Context context) {
        super(context, NAME, null, VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(CREATE_TODO_TABLE);
        db.execSQL(CREATE_USER_TABLE);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IFEXISTS" + TODO_TABLE);
        db.execSQL("DROP TABLE IFEXISTS" + USER_TABLE);
    }

    public void openDatabase() {
        db = this.getWritableDatabase();
    }

    public void insertTask(ToDoModel task) {
        ContentValues contentValues = new ContentValues();
        contentValues.put(TASK, task.getTask());
        contentValues.put(STATUS, 0);
        db.insert(TODO_TABLE, null, contentValues);
    }

    public boolean createUser(UserModel user) {
        ContentValues contentValues = new ContentValues();
        contentValues.put(USERNAME, user.getUsername());
        contentValues.put(PASSWORD, user.getPassword());
        long result = db.insert(USER_TABLE, null, contentValues);

        if(result == -1) {
            return false;
        }

        return true;
    }

    public List<ToDoModel> getAllTasks() {
        List<ToDoModel> taskList = new ArrayList<>();
        Cursor cursor = null;

        db.beginTransaction();
        try {
            cursor = db.query(TODO_TABLE, null, null, null, null, null, null);
            if (cursor != null) {
                if (cursor.moveToFirst()) {
                    do {
                        ToDoModel task = new ToDoModel();
                        task.setId(cursor.getInt(cursor.getColumnIndex(ID)));
                        task.setTask(cursor.getString(cursor.getColumnIndex(TASK)));
                        task.setStatus(cursor.getInt(cursor.getColumnIndex(STATUS)));
                        taskList.add(task);
                    } while (cursor.moveToNext());
                }
            }
        } finally {
            db.endTransaction();
            cursor.close();
        }

        return taskList;
    }

    public int authUser(UserModel user) {
        int userID = 0;
        Cursor cursor = null;

        String statement = "SELECT " + ID + " FROM " + USER_TABLE + " WHERE " + USERNAME + " = \"" + user.getUsername() + "\" AND " + PASSWORD + " = \"" + user.getPassword() + "\"";

        db.beginTransaction();
        try {
            System.out.println("try query : sebelum");
            cursor = db.rawQuery(statement, null);
            if (cursor != null) {
                if (cursor.moveToFirst()) {
                    do {
                        userID = cursor.getInt(cursor.getColumnIndexOrThrow(ID));
                    } while (cursor.moveToNext());
                }
            }
//            userID = cursor.getInt(cursor.getColumnIndex(ID));
            System.out.println(cursor);
            System.out.println("try query : sesudah");
        } finally {
            db.endTransaction();
        }
        System.out.println("return method auth " + String.valueOf(userID));

        return userID;
//        String statement = "SELECT * FROM " + USER_TABLE + " WHERE " + USERNAME + " = ? AND " + PASSWORD + " = ?";
//        String selectionArgs[] = {user.getUsername(), user.getPassword()};
//
//        Cursor cursor = this.db.rawQuery(statement, selectionArgs);
//
//        if(cursor.getCount() > 0) {
//            return cursor.getInt(cursor.getColumnIndex(ID));
//        }
//        return 0;
    }

    public void updateStatus(int id, int status) {
        ContentValues contentValues = new ContentValues();
        contentValues.put(STATUS, status);
        db.update(TODO_TABLE, contentValues, ID + "=?", new String[] {String.valueOf(id)});
    }

    public void updateTask(int id, String task) {
        ContentValues contentValues = new ContentValues();
        contentValues.put(TASK, task);
        db.update(TODO_TABLE, contentValues, ID + "=?", new String[] {String.valueOf(id)});
    }

    public void deleteTask(int id) {
        db.delete(TODO_TABLE, ID + "=?", new String[] {String.valueOf(id)});
    }
}
