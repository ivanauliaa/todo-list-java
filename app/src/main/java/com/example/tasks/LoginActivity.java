package com.example.tasks;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.example.tasks.Controller.AuthController;
import com.example.tasks.Model.UserModel;
import com.example.tasks.Util.DatabaseHandler;

public class LoginActivity extends AppCompatActivity {
    private EditText username;
    private EditText password;
    private Button login;
    private TextView register;

    private DatabaseHandler db;
    private AuthController authController;

    private SharedPreferences.Editor setData;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setData = this.getSharedPreferences("login_session", MODE_PRIVATE).edit();
        setContentView(R.layout.activity_login);

        username = findViewById(R.id.username_login_input);
        password = findViewById(R.id.password_login_input);
        login = findViewById(R.id.login_button);
        register = findViewById(R.id.register_redirect);

        db = new DatabaseHandler(this);
        db.openDatabase();
        authController = new AuthController(db);

        login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!username.getText().toString().equals("") && !password.getText().toString().equals("")) {
                    int userID = authController.login(new UserModel(username.getText().toString(), password.getText().toString()));

                    if (userID == 0) {
                        Toast.makeText(v.getContext(), "Something went wrong", Toast.LENGTH_SHORT).show();
                    } else {
                        System.out.println("onclick login OK");
                        setData.putString("USER_ID", String.valueOf(userID));
                        setData.apply();
                        Toast.makeText(getApplicationContext(), "Login berhasil", Toast.LENGTH_SHORT).show();
                        startActivity(new Intent(getApplicationContext(), MainActivity.class));
                        finish();
                    }
                } else {
                    Toast.makeText(getApplicationContext(), "Username dan password tidak boleh kosong", Toast.LENGTH_SHORT).show();
                }
            }
        });

        register.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(getApplicationContext(), RegisterActivity.class));
                finish();
            }
        });
    }
}